import javax.swing.*;

import java.awt.*;
public class GridLayoutDemo extends JFrame
{
	public GridLayoutDemo()
	{
		setLayout(new GridLayout(2,2));	
		setSize(new Dimension(200,150));
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		setLocationRelativeTo(null);
		
		JButton button1=new JButton("Button 1");
		JButton button2=new JButton("Button 2");
		JButton button3=new JButton("Button 3");
		JButton button4=new JButton("Button 4");
		JButton button5=new JButton("Button 5");
		JButton button6=new JButton("Button 6");
		JPanel panel1= new JPanel(new GridLayout(2,2));
		JPanel panel2= new JPanel(new GridLayout(2,2));
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel2.add(button4);
		panel2.add(button5);
		panel2.add(button6);
		add(panel2,BorderLayout.CENTER);
		add(panel1,BorderLayout.SOUTH);
		
		setTitle("GridLayout Demo");
	}
	public static void main(String[] args)
	{
		GridLayoutDemo gridLayoutTest = new GridLayoutDemo();
	}

}
